#
# Copyright (C) 2021 Bernardo Menicagli, Bruno Guindani
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
# USA
#
"""Create a predicting model for a specific EFP.

This module contains functions that parse a list of parameters and create
a model using the method specified by the user. After applying a Cross-Validation
on it, extracts a list of metric scores and test them against their thresholds.
"""

import amllibrary.sequence_data_processing
import json
import numpy as np
import os
from pathlib import Path
import shutil
from sklearn.base import RegressorMixin
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import cross_validate, KFold, LeavePOut
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import Pipeline

def create_model(metric_name, num_iterations, model_params, data, target):
    """Create a predicting model for the specified metric of interest.

    Parameters
    ----------
    metric_name : `str`
        The EFP name.
    num_iterations : `int`
        The number of iterations performed.
    model_params : `dict` [`str`, `str`]
        The keys corresponds to the parameter name. The values are the parameter
        value and they need to be parsed accordingly (e.g. as an `int` or a python `dict`).
    data : `pandas.DataFrame`
        A dataframe representing the matrix of predictors (i.e. software-knobs + input features).
    target : `pandas.DataFrame`
        A dataframe representing the matrix of target values (i.e. the metric of interest values).

    Returns
    -------
    model : `tuple` [`bool`, `sklearn.base.RegressorMixin`]
        (`True`, model) if a model was successfully found, with the best model returned.

        (`False`, fake_model) if no model was found, returns a fake model.

    Notes
    -----
    This function can be extended by adding new methods inside the proper
    section. It should be pretty straight forward as the cross validation
    is equal regardless of the hyper_parameters or the method type.
    """
    print(model_params)
    scoring_thresholds = json.loads(model_params['quality_threshold'].replace("'", "\"")) if 'quality_threshold' in model_params.keys() else {'r2':0.8, 'neg_mean_absolute_percentage_error':-0.1}
    aml_methods = model_params['aml_methods'].replace(" ","").split(",") if 'aml_methods' in model_params.keys() else ['LRRidge', 'XGBoost']

    # Save data to file
    data_temp = data.join(target)
    data_temp_path = Path("data_temp.csv")
    data_temp.to_csv(data_temp_path, index=False)

    # Create the estimator
    size_threshold = 30
    try:
        if data_temp.shape[0] < size_threshold:
            raise ValueError()
        print("Using a-MLlibrary for model building.")
        aml_output_fold = "output"
        aml_config_dict = {}
        aml_config_dict['General'] = {'run_num': 1, 'techniques': aml_methods,
            'hp_selection': 'KFold', 'folds': 5,
            'validation': 'HoldOut', 'hold_out_ratio': 0.2,
            'hyperparameter_tuning': 'Hyperopt', 'hyperopt_max_evals': 10, 'hyperopt_save_interval': 5,
            'y': target.name,
            'threads': 1  # number of parallel threads to execute experiments
        }
        aml_config_dict['DataPreparation'] = {'input_path': data_temp_path, 'normalization': True, 'product_max_degree': 4}
        aml_config_dict['LRRidge'] = {'alpha': ['loguniform(0.01,10)']}
        aml_config_dict['XGBoost'] = {'gamma': ['loguniform(0.1,10)'], 'learning_rate': ['loguniform(0.01,1)'],
                                      'min_child_weight': [1], 'max_depth': [100], 'n_estimators': [500]}
        aml_config_dict['RandomForest'] = {'min_samples_split': ['loguniform(0.01,0.5)'], 'max_depth': ['quniform(3,10,1)'],
                                           'criterion': ['mse'], 'max_features': ['auto'],
                                           'min_samples_leaf': [1], 'n_estimators': [500]}
        aml_config_dict['Stepwise'] = {'p_to_add': ['loguniform(0.001,1)'], 'p_to_remove': ['loguniform(0.001,1)'],
                                       'fit_intercept': [True], 'max_iter': [100]
        }
        seq_data_proc = amllibrary.sequence_data_processing.SequenceDataProcessing(
            aml_config_dict, debug=False, j=aml_config_dict['General']['threads'], output=aml_output_fold,
            generate_plots=False, self_check=False, details=False
        )
        regressor = seq_data_proc.process().get_regressor()
        # Remove temporary data file and output folder
        os.remove(data_temp_path)
        shutil.rmtree(aml_output_fold)
        estimator = Pipeline([('model', regressor)])
    except:
        # Either data size is below threshold, or a-MLlibrary has returned an error. Regardless, use a backup OLS estimator
        if data_temp.shape[0] < size_threshold:
            print("Data size [", data_temp.shape[0], "] is too small to use a-MLlibrary.", sep="")
        else:
            print("Error in a-MLlibrary.")
            # Remove temporary data file and output folder
            os.remove(data_temp_path)
            shutil.rmtree(aml_output_fold)
        print("Using an ordinary linear regression estimator instead.")
        estimator = Pipeline([('poly', PolynomialFeatures(degree=4)), ('model', LinearRegression(normalize=True))])

    # produce the cross validation
    num_cv_folds = int(model_params['num_cv_folds']) if 'num_cv_folds' in model_params.keys() else 5
    num_samples = len(data)
    ratio_threshold = 0.6
    set_ratio = (num_samples - num_cv_folds) / num_samples
    if set_ratio < ratio_threshold:
        print(f"Not enough samples [{num_samples}] to perform a full CV. Using Leave2Out stategy instead...")
        p = 2
        cv_strategy = LeavePOut(p) if p < num_samples else LeavePOut(1)
    else:
        cv_strategy = KFold(n_splits=num_cv_folds)
    splits_iterator = cv_strategy.split(data, target)

    scores_list = list(scoring_thresholds.keys())
    cv_results = cross_validate(estimator, data, target, cv=splits_iterator,return_estimator=True, scoring=scores_list, verbose=0, n_jobs=4)

    # check if the score thresholds are verified
    best_estimator = [0] * len(cv_results['estimator'])
    is_model_good = True
    for scoring, threshold in scoring_thresholds.items():
        scores_max_pos = np.argmax(cv_results['test_' + scoring])
        best_estimator[scores_max_pos] += 1
        scores_mean = np.mean(cv_results['test_' + scoring])
        print("Comparing "+scoring, " MEAN[", scores_mean, "] with", threshold)
        if scores_mean < threshold:
            print("The scoring", scoring, "has not verified the threshold limit.")
            is_model_good = False

    # since the model is good we can fit it
    return is_model_good, cv_results['estimator'][np.argmax(best_estimator)]
